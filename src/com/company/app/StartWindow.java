package com.company.app;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StartWindow extends JFrame {
    public StartWindow() {
        setLayout(new BorderLayout());
        setTitle("Snake start");
        setSize(400, 500);
        setLocation(400, 400);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);

        BorderLayout borderLayout = new BorderLayout();
        JLabel label = new JLabel("Snake");
        Font f = new Font("Arial", Font.CENTER_BASELINE, 26);
        label.setFont(f);
        add(label, BorderLayout.NORTH);

        JButton b1 = new JButton("Start");
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MainWindow();
                dispose();
            }
        });
        add(b1, BorderLayout.CENTER);
    }
}
