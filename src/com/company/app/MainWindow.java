package com.company.app;

import com.company.GameField;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JFrame {
    public MainWindow(){
        setTitle("Snake");
        setIconImage(new ImageIcon("logo.png").getImage());
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(320, 343);
        add(new GameField());
        setLocation(400,400);
        setVisible(true);
    }
}
