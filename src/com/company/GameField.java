package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;

public class GameField extends JPanel implements ActionListener {
    private final int SIZE = 320;
    private final int DOT_SIZE = 16;
    private final int ALL_DOTS = 400;
    private int delay;
    private boolean complexityIsSelected;
    private Image dot;
    private Image apple;
    private Image poisonedApple;
    private Image poisonedDot0;
    private Image poisonedDot1;
    private Image poisonedDot2;
    private int poisonedCount = 1;
    private int appleX;
    private int appleY;
    private int poisonedAppleX;
    private int poisonedAppleY;
    private int[] x = new int[ALL_DOTS];
    private int[] y = new int[ALL_DOTS];
    private int dots;
    private Timer timer;
    private boolean left = false;
    private boolean right = true;
    private boolean up = false;
    private boolean down = false;
    private boolean inGame = false;
    private boolean pause = false;

    public GameField(){
            setBackground(Color.BLACK);
            loadImages();
            intGame();
            addKeyListener(new KeyPress());
            setFocusable(true);
    }

    public void intGame(){
        dots = 3;
            for (int i = 0; i < dots; i++) {
                x[i] = 48 - i * DOT_SIZE;
                y[i] = 48;
            }
            timer = new Timer(delay, this);
            timer.start();
            createApple();
            createPoisonedApple();
    }

    public void createApple(){
        appleX = new Random().nextInt(19) * DOT_SIZE;
        appleY = new Random().nextInt(19) * DOT_SIZE;
    }

    public void createPoisonedApple() {
        poisonedAppleX = new Random().nextInt(19) * DOT_SIZE;
        poisonedAppleY = new Random().nextInt(19) * DOT_SIZE;
    }

    public void loadImages(){
        ImageIcon iia = new ImageIcon("apple.png");
        apple = iia.getImage();
        ImageIcon iid = new ImageIcon("dot.png");
        dot = iid.getImage();
        ImageIcon iic = new ImageIcon("poisonedApple.png");
        poisonedApple = iic.getImage();
        ImageIcon iif = new ImageIcon("poisonedDot0.png");
        poisonedDot0 = iif.getImage();
        ImageIcon iib = new ImageIcon("poisonedDot1.png");
        poisonedDot1 = iib.getImage();
        ImageIcon iie = new ImageIcon("poisonedDot2.png");
        poisonedDot2 = iie.getImage();
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Font f = new Font("Arial", Font.BOLD, 14);
        if(inGame){
            g.drawImage(apple, appleX, appleY, this);
            g.drawImage(poisonedApple, poisonedAppleX, poisonedAppleY, this);
            for (int i = 0; i < dots; i++) {
                g.drawImage(dot, x[i], y[i], this);
            }
            if(pause) {
                String str = "GAME IS ON PAUSE";
                setFont(f);
                g.drawString(str, 55, SIZE / 2);
            }
        } else {
            dot = poisonedDot0;
            poisonedCount = 1;
            String str1 = "Welcome to the Snake game!";
            String str2 = "PRESS ANY BUTTON FROM 1 TO 8";
            String str3 = "TO SELECT A COMPLEXITY";
            String str4 = "PRESS ENTER TO START";
            setFont(f);
            g.setColor(Color.WHITE);
            g.drawString(str1, 55, SIZE / 2 - 20);
            g.drawString(str2, 45, SIZE / 2 + 20);
            g.drawString(str3, 65, SIZE / 2 + 40);

            if(complexityIsSelected) {
                g.drawString(str4, 70, SIZE / 2 + 70);
            }
        }
    }

    public void move(){
        for (int i = dots; i > 0; i--) {
            x[i] = x[i - 1];
            y[i] = y[i - 1];
        }
        if(left)
            x[0] -= DOT_SIZE;
        if(right)
            x[0] += DOT_SIZE;
        if(up)
            y[0] -= DOT_SIZE;
        if(down)
            y[0] += DOT_SIZE;
    }

    public void checkApple(){
        if(x[0] == appleX && y[0] == appleY) {
            dots++;
            createApple();
        }

        for(int i = 1; i < dots; i++){
            if(x[i] == appleX && y[i] == appleY){
                createApple();
            }
        }
    }

    public void checkPoisonedApple() {
        if(x[0] == poisonedAppleX && y[0] == poisonedAppleY) {
            if(checkPoisonedCount()) {
                inGame = false;
            } else {
                poisonedCount++;
            }
            createPoisonedApple();
        }

        for(int i = 1; i < dots; i++) {
            if(x[i] == poisonedAppleX && y[i] == poisonedAppleY) {
                createPoisonedApple();
            }
        }

    }

    public boolean checkPoisonedCount() {
        if(poisonedCount == 1) {
            dot = poisonedDot1;
        }
        if(poisonedCount == 2) {
            dot = poisonedDot2;
        }
        if(poisonedCount == 3) {
            return true;
        } else {
            return false;
        }
    }

    public void checkCollision(){
        for (int i = dots; i > 0 ; i--) {
            if(i > 4 && x[0] == x[i - 1] && y[0] == y[i - 1])
                inGame = false;
        }

        if(dots <= 0) {
            inGame = false;
        }

        if(x[0] > (SIZE - 32))
                x[0] = 0;

            if(x[0] < 0)
                x[0] = SIZE - 32;

            if(y[0] > (SIZE - 32))
                y[0] = 0;

            if(y[0] < 0)
                y[0] = SIZE - 32;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(inGame){
            checkApple();
            checkPoisonedApple();
            checkCollision();
            move();
        } else {
            timer.stop();
        }
        repaint();
    }

    class KeyPress extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e){
            super.keyPressed(e);
            int key = e.getKeyCode();
            if(key == KeyEvent.VK_A && !right){
                left = true;
                up = false;
                down = false;
            }
            if(key == KeyEvent.VK_D && !left){
                right = true;
                up = false;
                down = false;
            }
            if(key == KeyEvent.VK_W && !down){
                up = true;
                right = false;
                left = false;
            }
            if(key == KeyEvent.VK_S && !up){
                down = true;
                right = false;
                left = false;
            }
            if(key == KeyEvent.VK_ENTER && !inGame) {
                intGame();
                up = false;
                down = false;
                left = false;
                right = true;
                timer.start();
                inGame = true;
            }


            //change a complexity
            if(!inGame && key == KeyEvent.VK_1) {
                delay = 450;
                complexityIsSelected = true;
            }
            if(!inGame && key == KeyEvent.VK_2) {
                delay = 400;
                complexityIsSelected = true;
            }
            if(!inGame && key == KeyEvent.VK_3) {
                delay = 350;
                complexityIsSelected = true;
            }
            if(!inGame && key == KeyEvent.VK_4) {
                delay = 300;
                complexityIsSelected = true;
            }
            if(!inGame && key == KeyEvent.VK_5) {
                delay = 250;
                complexityIsSelected = true;
            }
            if(!inGame && key == KeyEvent.VK_6) {
                delay = 200;
                complexityIsSelected = true;
            }
            if(!inGame && key == KeyEvent.VK_7) {
                delay = 150;
                complexityIsSelected = true;
            }
            if(!inGame && key == KeyEvent.VK_8) {
                delay = 100;
                complexityIsSelected = true;
            }

            //pause menu open
            if(inGame && key == KeyEvent.VK_ESCAPE) {
                pause = true;
            }
            //close pause menu
            if(inGame && pause && key == KeyEvent.VK_ESCAPE) {
                pause = false;
            }
        }
    }
}